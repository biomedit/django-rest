import React from "react";
import { createRoot } from "react-dom/client";
import App from "./components/App";

const container = document.getElementById("app");
// @ts-expect-error We know the app element exists
const root = createRoot(container);
root.render(<App />);
