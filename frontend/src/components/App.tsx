import React, { useState, useEffect } from "react";

interface Project {
  id: number;
  key: string;
  name: string;
}

const App = () => {
  const [data, setData] = useState<Project[]>();

  useEffect(() => {
    fetch("api/project")
      .then((response) => response.json())
      .then((data) => setData(data));
  });

  return (
    data && (
      <ul>
        {data.map((project) => {
          return (
            <li key={project.id}>
              {project.key} - {project.name}
            </li>
          );
        })}
      </ul>
    )
  );
};

export default App;
