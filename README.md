# Assessment Exercise

This skeleton project was generated following the instructions provided
[here](https://www.valentinog.com/blog/drf/). It's a simple
[django](https://www.djangoproject.com/) - [React](https://reactjs.org/)
project, composed of a single entity `Project`.

## Requirements

- Python >= 3.12
- Node >= 20
- npm >= 10.5.2

## Starting the application

1. Install backend and frontend requirements.
2. Set up the DB with `./manage.py migrate`.
3. Run the backend server with `./manage.py runserver`.
4. From the `frontend` folder, build the static files with `npm run dev`.
5. Create some projects by accessing <http://localhost:8000/api/project/>.
6. Head over to <http://127.0.0.1:8000/> to list the projects in your React
   client.

## Your mission, should you choose to accept it

Each project must include a list of users.

You are required to implement a User entity. A `User` is typically composed of
the following properties: first name, last name, and email. It should be
possible to attach one or more users to a project.

The task is simple and generic, so that you could spend time on the area you
feel the most confident with. However, the following minimum requirements must
be met:

- On the _frontend_, we should be able to list each project with the associated
  users.
- Some tests.

Please do not spend more than half a day to one full day on this task. Our goal
is to see how you work. Impress us with your skills! Good luck!

## Submitting your solution

Your solution should be submitted as a merge request from your **Private** fork
of this repository - feel free to go through multiple iterations!

**For new submissions**: Your merge request should NOT target this repository
but rather your own private fork of it. Additionally, please grant owner or
maintainer access to @simone.guzzi.

Note: To fork the repository and submit a merge request, you will need to create
an account on GitLab.com. If you strongly prefer not to do so, you may use a
Git-based version control platform of your choice. Regardless of the platform
you choose, we still require your solution to be submitted as a merge request on
your chosen platform. Please ensure you share the repository link with us before
the exercise deadline.
